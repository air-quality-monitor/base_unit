/*
 *  UI pages needed for the screen
 */

#include "pages.h"


void setPage(uint8_t page_number) {
  /*
   *  Change the whole page at once to a different UIPage
   *  page_number specifies which page to change to
   *  Selected line should reset to 0 as there may not be more options
   */

  char str_buf[16];

  // Update title
  if (page_number != PAGE_GRAPH) { // Normal page
    strcpy(str_buf, pages[page_number].name);
    updateLine(0, str_buf);
  }
  else {                           // Graph page
    // Update title with this graph's name
    strcpy(str_buf, pages[0].options[selected_line]);
    updateLine(0, str_buf);
  }

  // Update line by line
  for (uint8_t i=0; i<4; i++) {
    strcpy(str_buf, pages[page_number].options[i]);
    updateLine(i+1, str_buf);
  }

  // Send graph data if needed
  if (page_number == PAGE_GRAPH) {
    switch(selected_line+1) {
    case GRAPH_PM2_5:
      graphSavedData(true);
      break;
    case GRAPH_PM10:
      graphSavedData(false);
      break;
    case GRAPH_HISTORY:
      graphHistory();
      break;
    }
  }
  // Clear graph just in case
  else if (page_number == 0) {
    i2c_start();
    i2c_write(DISPLAY_ADDRESS);
    i2c_write(CMD_GRAPH_CLEAR);
    i2c_stop();
  }

  // Set selected line to 0
  selected_line = 0;
  menuSelect(selected_line);

  // Keep track of current page
  current_page = page_number;
}
