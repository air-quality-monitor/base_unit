/*
 *  Analogue joystick library
 *  2018 George Bryant
 *
 *  Joystick wired with x-axis on RA0, y-axis on RA1, button on RA2
 *  Joystick PCB has a built-in pullup resistor for the button.
 */


#include <joystick.h>


void setupJoystick(void) {
  // ADC on A0 and A1 for joystick
  setup_adc_ports(sAN0 | sAN1);
  setup_adc(ADC_CLOCK_DIV_16);
}


direction joystickDir() {
  direction new_dir = CENTRE;
  int8_t joy_x, joy_y;

  set_adc_channel(0);

  delay_us(10);
  joy_x = thresholdCheck(read_adc());

  set_adc_channel(1);
  delay_us(10);
  joy_y = thresholdCheck(read_adc());

  // Order of priority: Up, down, left, right
  if (joy_x == 1) new_dir = UP;
  else if (joy_x == -1) new_dir = DOWN;
  else if (joy_y == 1) new_dir = LEFT;
  else if (joy_y == -1) new_dir = RIGHT;

  return new_dir;
}


int8_t thresholdCheck(uint8_t val) {
  /*
   *  Check whether joystick value deviates from centre by more than threshold
   *  Return -1 if below negative threshold, 0 if between or 1 if positive
   */
  if (val < (JOY_CENTRE - JOY_THRESHOLD)) return -1;
  else if (val > (JOY_CENTRE + JOY_THRESHOLD)) return 1;
  else return 0;
}

