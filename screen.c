/*
 *  ST7920 screen library
 *  George Bryant 2018
 *
 *  This communicates over i2c with an Arduino.
 *  The Arduino controls the screen over SPI using the u8g2 library.
 *  This side of the program isn't dependent on the screen resolution.
 */


#include <screen.h>


void updateLine(uint8_t line, char * name) {
  /*
   *  Update a line of the on-screen menu
   *  0th line max length 11 characters
   *  Other lines max length 16 characters
   */

  i2c_start();
  i2c_write(DISPLAY_ADDRESS);
  i2c_write(CMD_SET_LINE);
  i2c_write(line);
  for (int i=0; i<16; i++) {
    if (name[i] != '\0') i2c_write(name[i]);
    else break;
  }
  i2c_write('\0');
  i2c_stop();
}


void setGraphPage(uint8_t * buf, uint8_t page_index) {
  /*
   *  Set one page of the graph on-screen
   *  buf[25] is a 25-byte buffer for the graph data
   *  page_index(0-4) specifies which section of the graph is being written
   */

  i2c_start();
  i2c_write(DISPLAY_ADDRESS);
  i2c_write(CMD_GRAPH_START);
  i2c_write(page_index);
  for (int i=0; i<25; i++) {
    i2c_write(buf[i]);
  }
  i2c_write(CMD_GRAPH_END);
  i2c_stop();
}


void setTime(uint8_t hour, uint8_t minute) {
  /*
   *  Send time to Arduino to display in top right or in centre
   */

  i2c_start();
  i2c_write(DISPLAY_ADDRESS);
  i2c_write(CMD_SET_TIME);
  i2c_write(hour);
  i2c_write(minute);
  i2c_stop();
}


void menuSelect(uint8_t line) {
  /*
   *  Send a line select command to the Arduino
   *  This will change which line is highlighted
   */

  i2c_start();
  i2c_write(DISPLAY_ADDRESS);
  i2c_write(CMD_SELECT);
  i2c_write(line);
  i2c_stop();
}


void menuMove(uint8_t dir) {
  /*
   *  Send a move command and direction to the Arduino
   */

  i2c_start();
  i2c_write(DISPLAY_ADDRESS);
  i2c_write(CMD_MOVE);
  i2c_write(dir);
  i2c_stop();
}
