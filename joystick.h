/*
 *  Analogue joystick library
 *  2018 George Bryant
 *
 *  Joystick wired with x-axis on RA0, y-axis on RA1, button on RA2
 *  Joystick PCB has a built-in pullup resistor for the button.
 */


#include <stdint.h>
#include <stdbool.h>


// Joystick parameters
#define JOY_CENTRE 128
#define JOY_THRESHOLD 100
//#define JOY_DEBOUNCE 100
//#define JOY_REPEAT 500
//#define BUTTON_DEBOUNCE 100
#define BUTTON_PIN PIN_A2


// Simplified directions: no diagonals
typedef enum {CENTRE, UP, DOWN, LEFT, RIGHT} direction;


void setupJoystick(void);
direction joystickDir(void);
int8_t thresholdCheck(uint8_t val);
