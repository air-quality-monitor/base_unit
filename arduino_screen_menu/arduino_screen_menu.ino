/*
 *  Arduino code to drive ST7920 128x64 screen in SPI mode
 *
 *  Wiring:
 *    LCD     Arduino
 *    ---------------
 *    GND     GND
 *    VDD     5V
 *    RS      10    - Chip Select (can probably just be pulled high)
 *    R/W     11    - MOSI
 *    E       13    - SCLK
 *    PSB     GND   - LOW to select SPI instead of parallel
 *    RST     8     - Register select
 *    BLA     5V
 *    BLK     GND
 */


#include <U8g2lib.h>
#include <Wire.h>

// Right shifted once from the 0xB0 set on the PIC
#define I2C_ADDRESS 0x58

// Commands for I2C
// MOVE, SELECT and BACK are for menu navigation
// CLEAR_SCREEN will clear the entire screen
// MENU_NAME and MENU_OPTION will set those lines of the menu on screen
// TIME will show the time in the top right corner
// DATETIME will show the time and date in the centre of the screen
// GRAPH_START, GRAPH_END are for sending graph data
//typedef enum {MOVE, SELECT, BACK,
//              CLEAR_SCREEN,
//              MENU_NAME, MENU_OPTION,
//              SET_TIME, DATETIME,
//              GRAPH_START, GRAPH_END} command;
#define CMD_MOVE            0   // Navigation directions
#define CMD_SELECT          1   // Navigation select - maybe unused
#define CMD_BACK            2   // Back in menu - maybe unused?
#define CMD_CLEAR           3   // Clear screen
#define CMD_SET_LINE        4   // Set line of screen
#define CMD_SET_TIME        5   // Set time in top right corner
#define CMD_SHOW_DATETIME   6   // Show date and time in screen centre
#define CMD_GRAPH_START     7   // Start graph data stream
#define CMD_GRAPH_END       8   // End graph data stream
#define CMD_GRAPH_CLEAR     9   // Clear the graph and buffer


// LCD dimensions
#define LCD_W 128
#define LCD_H 64
#define LCD_COLS 16
#define LCD_ROWS 5

// Limit graph data length
#define GRAPH_MAX 125
#define GRAPH_TRANSFER 25  // Bytes transferred in one go - less than 32

// Simplified directions: no diagonals
typedef enum {CENTRE, UP, DOWN, LEFT, RIGHT} direction;


typedef struct ui_page_struct {
  char name[12];
  char options[4][16];
} UIPage;


void draw();


// Clock, data, CS, reset
//U8G2_ST7920_128X64_1_HW_SPI u8g(U8G2_R0, 13,  11,  10,  8);
U8G2_ST7920_128X64_1_HW_SPI u8g2(U8G2_R2, /*CS:*/ 10, /*Reset:*/ 8);

int yPos=0;
uint8_t line_height = LCD_H / LCD_ROWS;
UIPage current_page;
uint8_t selected_line = 0, current_hour = 0, current_minute = 0;
uint8_t graph_buf[GRAPH_MAX];   // Buffer for graph data

// Store current mode
typedef enum {NORMAL, GRAPH, CLOCK} mode;
mode current_mode = NORMAL;


void moveCursor(direction dir) {

  if (dir == UP) selected_line += 1;
  else if (dir == DOWN) selected_line -= 1;

  if (selected_line > 250) selected_line = 0;
  else if (selected_line > 3) selected_line = 3;

  refreshDisplay();
}


void getGraphData() {
  /*
   *  Receive one page of data data for the graph
   *
   *  I2C transfer limit on arduino is 32 bytes,
   *  so transfer only 25 bytes at a time (plus start, index and end command)
   *  `page` refers to which group of 25 points in the buffer is being sent
   */

  uint8_t tmp, page;
  uint8_t i;

  current_mode = GRAPH;

  // Receive page (0-4)
  page = Wire.read();

  // Receive 25 bytes and put into graph_buf
  // 1 byte extra so it's not left with the END command afterwards
  for (i=page*GRAPH_TRANSFER; i<page*GRAPH_TRANSFER+26; i++) {
    tmp = Wire.read();

    // Break if transmission ended early
    if (tmp == CMD_GRAPH_END) break;

    // Scale and cap graph at 80
    if (tmp > 80) tmp = 80;
    graph_buf[i] = map(tmp, 0, 80, 0, 255);
  }
}


void updateLine() {
  /*
   *  Update the text of a specific line
   *  Lines numbered 0-4 (title, 4 options)
   */

  uint8_t line;
  uint8_t buf[16];

  line = Wire.read();         // Get line number

  for (int i=0; i<16; i++) {  // Get line text
    buf[i] = Wire.read();

    if (buf[i] == '\0') break;
  }

  if (line == 0) {  // Title line
    strncpy(current_page.name, buf, 12);
  }
  else {
    // Copy line to current page
    strncpy(current_page.options[line-1], buf, 16);
  }
}


void receiveEvent(int n) {
  //String message = "";
  uint8_t command_type;
  direction dir;

  command_type = Wire.read();

  switch(command_type) {
  case CMD_MOVE:
    dir = Wire.read();
    moveCursor(dir);
    break;

  case CMD_SELECT:
    selected_line = Wire.read();

    refreshDisplay();
    break;

  case CMD_CLEAR:
    u8g2.clear();
    break;

  case CMD_SET_LINE:
    updateLine();

    refreshDisplay();
    break;

  case CMD_SET_TIME:
    // Leave current mode as-is
    current_hour = Wire.read();
    current_minute = Wire.read();

    refreshDisplay();
    break;

  case CMD_SHOW_DATETIME:
    u8g2.clear();
    current_mode = CLOCK;
    refreshDisplay();
    break;

  case CMD_GRAPH_START:
    getGraphData();
    refreshDisplay();
    break;

  case CMD_GRAPH_CLEAR:
    for (int i=0; i<GRAPH_MAX; i++) graph_buf[i] = 0;
    refreshDisplay();
    current_mode = NORMAL;
    refreshDisplay();
    break;
  }
}


void refreshDisplay() {
  u8g2.firstPage();
  do {
    draw();
  } while (u8g2.nextPage());
}


void setup() {
  Serial.begin(9600);

  u8g2.begin();
  u8g2.setFont(u8g_font_7x13);
  u8g2.setColorIndex(1); // Instructs the display to draw with a pixel on. 

  // Join i2c bus as slave and register receive callback
  Wire.begin(I2C_ADDRESS);
  Wire.onReceive(receiveEvent);

  current_mode = NORMAL;

  refreshDisplay();
}


void loop() {
}



void draw(){
  //u8g2.drawStr( 0, yPos, "Hello World");
  uint8_t xpos=0, ypos=9;

  if (current_mode != CLOCK) {
    // Draw title
    u8g2.setFont(u8g_font_7x13B);
    u8g2.drawStr(0, 9, current_page.name);

    // Draw time
    u8g2.setFont(u8g_font_7x13);
    u8g2.setDrawColor(1);
    char buf[3] = {'\0','\0','\0'};
    String(current_hour).toCharArray(buf, 3);
    if (buf[1] == '\0') {
      buf[1] = buf[0];
      buf[0] = '0';
    }
    u8g2.drawStr(96, 9, buf);     // Draw hours
    u8g2.drawStr(108, 8, ":");    // Draw colon
    String(current_minute).toCharArray(buf, 3);
    if (buf[1] == '\0') {
      buf[1] = buf[0];
      buf[0] = '0';
    }
    u8g2.drawStr(114, 9, buf);    // Draw minutes
  }

  if (current_mode != CLOCK) {
    // Draw options
    u8g2.setFont(u8g_font_7x13);
    u8g2.setDrawColor(1);
    for (uint8_t i=0; i<LCD_ROWS-1; i++) {
      xpos = 10;
      ypos = 22 + 13*i;

      // Show selected line inverted
      if (i == selected_line) {
        // Draw box behind line
        u8g2.drawBox(0, ypos-10, LCD_W, 13);

        u8g2.setDrawColor(0);
        u8g2.drawStr(xpos, ypos, current_page.options[i]);
        u8g2.setDrawColor(1);
      }
      else {
        u8g2.drawStr(xpos, ypos, current_page.options[i]);
      }
    }
    //}
    if (current_mode == GRAPH) {
      //Draw options
      u8g2.setDrawColor(1);

      // Draw axes
      u8g2.drawLine(1, 30, 1, 62);
      u8g2.drawLine(1, 62, 126, 62);

      // Draw graph
      for (int i=0; i<GRAPH_MAX; i++) {
        // Draw pixel for each graph point
        // Shift right thrice to divide by 8
        u8g2.drawPixel(i+2, 62-(graph_buf[i] >> 3));
      }
    }
  }
  else if (current_mode == CLOCK) {
    // Draw time
    u8g2.setFont(u8g_font_7x13);
    u8g2.setDrawColor(1);
    char buf[3] = {'\0','\0','\0'};

    // Calculate hours
    String(current_hour).toCharArray(buf, 3);
    if (buf[1] == '\0') {
      buf[1] = buf[0];
      buf[0] = '0';
    }
    u8g2.drawStr(49, 36, buf);     // Draw hours

    u8g2.drawStr(61, 35, ":");    // Draw colon

    // Calculate minutes
    String(current_minute).toCharArray(buf, 3);
    if (buf[1] == '\0') {
      buf[1] = buf[0];
      buf[0] = '0';
    }
    u8g2.drawStr(67, 36, buf);    // Draw minutes
  }
}
