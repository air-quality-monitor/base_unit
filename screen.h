/*
 *  ST7920 screen library
 *  George Bryant 2018
 *
 *  This communicates over i2c with an Arduino.
 *  The Arduino controls the screen over SPI using the u8g2 library.
 *  This side of the program isn't dependent on the screen resolution.
 */


#define I2C_SDA  PIN_B1
#define I2C_SCL  PIN_B4
#define DISPLAY_ADDRESS 0xB0
#use i2c(master, sda=I2C_SDA, scl=I2C_SCL, FAST)


// Commands for I2C
#define CMD_MOVE            0   // Navigation directions
#define CMD_SELECT          1   // Navigation select - maybe unused
#define CMD_BACK            2   // Back in menu - maybe unused?
#define CMD_CLEAR           3   // Clear screen
#define CMD_SET_LINE        4   // Set line of screen
#define CMD_SET_TIME        5   // Set time in top right corner
#define CMD_SHOW_DATETIME   6   // Show date and time in screen centre
#define CMD_GRAPH_START     7   // Start graph data stream
#define CMD_GRAPH_END       8   // End graph data stream
#define CMD_GRAPH_CLEAR     9   // Clear the graph and buffer

void updateLine(uint8_t line, char * name);
void setGraphPage(void);
void setTime(uint8_t hour, uint8_t minute);
void menuSelect(uint8_t line);
void menuMove(uint8_t dir);


uint8_t selected_line = 0;
