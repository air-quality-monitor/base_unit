/*
 *  Test of screen menu controlling using PIC to control
 *  Arduino screen controller
 */

#include <16F1827.h>
#DEVICE ADC=8                   // Select 8-bit mode

#fuses MCLR,INTRC_IO,NOWDT,NOPROTECT,NOBROWNOUT,NOPUT
#use delay(clock=8000000)

#use rs232(baud=38400, xmit=PIN_A1, STREAM=PC)


#include <stdint.h>
#include <stdbool.h>

// Prototype to be accessed by pages.c
void graphSavedData(bool pm2_5);
void graphHistory(void);

#include "joystick.c"
#include "screen.c"
#include "pages.c"
#include "eeprom/eeprom.c"
#include "ds1307/ds1307.c"


// Function prototypes
bool updateButton(bool was_pressed, bool is_pressed);
direction updateDir(direction old_dir, direction new_dir);
uint16_t timestamp(Measurement m);
void updateConnected(void);
void promptTimeUpdate(void);


// Global variables
bool navigation_enabled = true;  // Allow disabling of movement+selection
byte hour, min, sec;             // Store current time
bool time_update_ready = false;  // Signal that the time can be updated on screen


bool updateButton(bool was_pressed, bool is_pressed) {
  /*
   *  Send any select button presses to the Arduino
   */

  if (is_pressed != was_pressed) {
    if (is_pressed) setPage(pages[current_page].links[selected_line]);

    was_pressed = is_pressed;
  }

  return was_pressed;
}


direction updateDir(direction old_dir, direction new_dir) {
  /*
   *  Send any direction changes to the Arduino
   *  Also includes test code calling other functions
   */

  if (new_dir != old_dir) {
    //fprintf(PC, "x: %d, y: %d\r\n", joy_x, joy_y);
    char str_buf[16];

    switch(new_dir) {
    case UP:
      selected_line++;
      if (selected_line > 3) selected_line = 3;
      menuSelect(selected_line);
      break;
    case DOWN:
      selected_line--;
      if (selected_line == 255) selected_line = 0;
      menuSelect(selected_line);
      break;
    case LEFT:
      //setTime(00, 00);
      break;
    case RIGHT:
      break;
    }

    old_dir = new_dir;

    fprintf(PC, "\r\n%d", new_dir);
  }

  return old_dir;
}


uint16_t timestamp(Measurement m) {
  /*
   *  Generate 16-bit timestamp from a measurement's time
   */
  return (uint16_t) m.hour * 30*60
       + (uint16_t) m.minute * 30
       + (uint16_t) m.second / 2;  // Seconds divided by 2 to fit day in 16 bits
}


void graphSavedData(bool pm2_5) { // pm2_5 or pm10
  /*
   *  Get graph data from the EEPROM and send it to the Arduino
   */

  // 25 byte buffer - 1 "page" of graph on Arduino
  // Full width is 125
  uint8_t data_buf[25];
  uint8_t count;
  float average;
  uint16_t t_start, t_end, t_delta, j;
  uint16_t search_address = EEPROM_DATA_OFFSET, end_address;
  Measurement m;

  // Find start and end times (assumes same day)
  m = loadMeasurement(EEPROM_DATA_OFFSET);  // First saved measurement
  t_start = timestamp(m);

  m = loadMeasurement(loadMeasurementAddress()-EEPROM_MEASUREMENT_SIZE); // Last saved measurement
  t_end = timestamp(m);

  t_delta = t_end - t_start;    // Time between start and end measurements


  // Load data into buffer

  // Re-use t_end to mean "end of the current pixel"
  // Re-use t_delta to mean "time per pixel"
  // Re-use t_start to mean "start of the current page"
  t_delta = t_delta / 125;

  // Iterate through pixels of the graph
  // Unfortunately this does load each measurement twice so it will be slow
  for (uint8_t page=0; page<5; page++) { // Iterate through the 5 pages of the graph
    for (uint8_t i=0; i<25; i++) {       // Iterate through pixels of this page
      t_end = t_start + t_delta*(i+1);   // End time of this pixel
      count = 0;                         // Reset count of this pixel's measurements

      // Search ahead until one occurs after t_end
      for (j=search_address;
          j<loadMeasurementAddress();
          j+=EEPROM_MEASUREMENT_SIZE) {
        m = loadMeasurement(j);
        count++;  // Number of measurements in this pixel
        if (timestamp(m) >= t_end) break;
      }

      // Save j, which is address of first measurement in next pixel
      end_address = j;

      // Load measurements in this pixel into buffer
      average = 0;                // Store average for this pixel
      for (j=search_address; j<=end_address;j+=EEPROM_MEASUREMENT_SIZE) {
        m = loadMeasurement(j);
        if (pm2_5) average += (float) m.PM2_5 / (float) count;
        else       average += (float) m.PM10  / (float) count;
      }
      data_buf[i] = (uint8_t) average;  // Save average in buffer

      // Update search start address for next iteration
      search_address = end_address;
    }

    // Print buffer
    //for (i=0; i<25; i++) {
    //  fprintf(PC, "%u", data_buf[i]);
    //}

    // Send data to Arduino
    setGraphPage((uint8_t*) data_buf, page);

    // Increase t_start to be the end of the current buffer
    t_start += t_delta * 25;
  }
}


void graphHistory(void) {
  /*
   *  Display the last 5 days' average PM2.5 level
   *  This is only a mockup - for the real thing, the date and average
   *  measurement would be stored in EEPROM on the PIC.
   */

  for (uint8_t page=0; page<5; page++) {
    i2c_start();
    i2c_write(DISPLAY_ADDRESS);
    i2c_write(CMD_GRAPH_START);
    i2c_write(page);
    for (uint8_t i=0; i<25; i++) {
      i2c_write((page+121)*(page+54)-53); // just a varying number
    }
    i2c_write(CMD_GRAPH_END);
    i2c_stop();
  }
}


#INT_EXT
void updateConnected(void) {
  /*
   *  Triggered when storage module connected or disconnected
   *  Update contents of screen accordingly
   */
  // Debounce delay
  delay_ms(100);

  // Check whether storage connected or disconnected
  bool connected = !input(PIN_B0);

  if (connected) {
    setPage(PAGE_MAIN);
    enable_interrupts(INT_TIMER1);   // Allow time to be updated again
    navigation_enabled = true;       // Re-enable joystick
  }
  else {
    setPage(PAGE_DISCONNECTED);
    setTime(0,0);                    // Once disconnected, time is unknown
    disable_interrupts(INT_TIMER1);  // Stop the time being updated to a nonsense value
    navigation_enabled = false;      // Disable joystick
  }
}

#INT_TIMER1
void promptTimeUpdate(void) {
  /*
   *  Every 10s (40 T1 cycles), prompt the main loop to check the time
   */

  // Further divide Timer1 with a counter
  static uint8_t t1_counter;
  t1_counter += 1;

  // Only update time every 40th cycle (10s)
  if (t1_counter == 40) {
    time_update_ready = true;
    t1_counter = 0;
  }

  set_timer1(3036);             // 0.25s overflow
}


void main() {
  fputs("Starting\r\n", PC);

  setupJoystick();
  navigation_enabled = true;

  direction old_dir, new_dir;
  //command display_command;
  uint32_t current_time;
  bool button_was_pressed, button_pressed;

  // Set up time
  ds1307_init();
  delay_ms(100);
  ds1307_get_time(hour, min, sec); // Store in global variables
  time_update_ready = false;
  //hour = 0;
  //min = 0;

  // Use T1 to check time every 10 seconds
  setup_timer_1(T1_INTERNAL | T1_DIV_BY_8);
  set_timer1(3036);             // 0.25s overflow

  // Set screen to main menu
  delay_ms(500);                   // Give Arduino time to start
  setPage(PAGE_MAIN);

  setTime(00, 00);

  enable_interrupts(INT_TIMER1);
  enable_interrupts(INT_EXT);      // ISR checks whether storage module is connected
  enable_interrupts(GLOBAL);

  while(1) {
    if (navigation_enabled) {
      new_dir = joystickDir();
      button_pressed = !input(BUTTON_PIN);

      // Joystick and button don't seem to need much debounce
      button_was_pressed = updateButton(button_was_pressed, button_pressed);
      old_dir = updateDir(old_dir, new_dir);
    }

    if (time_update_ready) {
      // Not in ISR to avoid interrupting existing write to screen
      ds1307_get_time(hour, min, sec);

      setTime(hour, min);

      time_update_ready = false;
    }
  }
}
