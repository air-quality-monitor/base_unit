/*
 *  UI pages needed for the screen
 */


// Struct type to store page data
typedef struct ui_page_struct {
  char name[12];                // Title of page
  char options[4][16];          // Names of options on page
  uint8_t links[4];             // Number of page to link to. 255 means no link
} UIPage;
// Using numbers for links instead of pointers,
// because it would be impossible to point constants at each other.


// Store current page number as int to match links
// Initialise as 0 (main)
uint8_t current_page = 0;

// When showing a graph, store graph number in current_graph
uint8_t current_graph = 0;

#define PAGE_MAIN 0
#define PAGE_GRAPH 1            // Page graphs located on
#define PAGE_DISCONNECTED 2
#define GRAPH_NONE 0
#define GRAPH_PM2_5 1
#define GRAPH_PM10 2
#define GRAPH_HISTORY 3


// Constant, so must be given values immediately
const UIPage pages[] = {
  {                             // Page 0 (Main)
    "Air Monitor",              // Name
    {                           // Options
      "PM2.5 Graph",
      "PM10 Graph",
      "History"
    },
    {                           // Links
      1, 1, 1, 255              // Graphs all link to same page
    }
  },

  {                             // Page 1 (Graph)
    "",                         // Name different for each graph
    {                           // Only one option - graph takes up bottom
      "Back"
    },
    {
      0, 255, 255, 255
    }
  },

  {                             // Page 2 (Disconnected)
    "Connect",                  // Name
    {
      "Connect storage",
      "  module to",
      "  continue"
    },
    {                           // Links - no destination
      255, 255, 255, 255
    }
  },
};


void setPage(uint8_t page_number);
